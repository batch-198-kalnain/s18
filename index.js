function additionOfTwoNumber(num1, num2) {
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1+num2);
}

additionOfTwoNumber(5,15);

function subtractionOfTwoNumber (num1, num2) {
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
}

subtractionOfTwoNumber(20,5);

function productOfTwoNumber (num1, num2) {
	console.log("Displayed product of " + num1 + " and " + num2);
	return num1 * num2;	
}

let product = productOfTwoNumber(50,10);
console.log(product);

function quotientOfTwoNumber (num1, num2) {
	console.log("Displayed quotient of " + num1 + " and " + num2);
	return num1 / num2;	
}

let quotient = quotientOfTwoNumber(50,10);
console.log(quotient);

function calculateAreaOfCircle(radius) {
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	let result = 3.1416*(radius ** 2);
	return result;
}

let circleArea = calculateAreaOfCircle(15);
console.log(circleArea);

function average(num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4);
	let averageResult = (num1 + num2 + num3 + num4) / 4;
	return averageResult; 
}

let averageVar = average(20, 40, 60, 80);
console.log(averageVar);

function checkPassingPercentage(num1, num2) {
	console.log("Is " + num1 + "/" + num2 + " a passing score?");
	let percentageResult = (num1 / num2) * 100;
	let isPassed = percentageResult >= 75;
	return isPassed;
}

isPassingScore = checkPassingPercentage(38, 50);
console.log(isPassingScore);